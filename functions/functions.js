"use strict";

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

const bucket = admin.storage().bucket();

const util = require('util');
const path = require('path');

const SPEECH = require('@google-cloud/speech');
const speech = new SPEECH.v1p1beta1.SpeechClient();

const mm = require('music-metadata');

//https://cloud.google.com/nodejs/docs/reference/speech/2.1.x/google.cloud.speech.v1p1beta1#.AudioEncoding
const formats = {
  'Ogg/Opus': 'OGG_OPUS',
  'flac': 'FLAC',
  'WAVE/PCM': 'LINEAR16'
}


/*
https://cloud.google.com/nodejs/docs/reference/speech/2.1.x/v1.SpeechClient#longRunningRecognize
https://cloud.google.com/speech-to-text/docs/async-recognize
https://cloud.google.com/speech-to-text/docs/reference/rest/v1/RecognitionConfig#AudioEncoding
https://cloud.google.com/speech-to-text/docs/multi-channel#speech-multi-channel-nodejs
https://cloud.google.com/speech-to-text/docs/recognition-metadata
*/

exports.processAudio = functions.region('europe-west1').runWith({timeoutSeconds: 540})
.storage.object().onFinalize(async (object) => {
  try {
    if (!object.contentType.startsWith('audio/')) return console.log('Invalid "contentType"!');

    let config = {
      encoding: 'FLAC',
      sampleRateHertz: 44100,
      languageCode: 'en-US',
      enableAutomaticPunctuation: true,
      useEnhanced: true,
      model: 'default',
      metadata: {
        originalMediaType: 'AUDIO',
        interactionType: 'PRESENTATION'
      }
    }

    let initial_response = await speech.longRunningRecognize({
      audio: {
        uri: `gs://${object.bucket}/${object.name}`,
      },
      config: config
    });
    let operation = initial_response[0];
    let initialApiResponse = initial_response[1];
    console.log(util.inspect(operation));
    console.log(util.inspect(initialApiResponse));

    // Get a Promise representation of the final result of the job
    let final_response = await operation.promise();

    // The final result of the operation.
    let response = final_response[0];
    console.log(util.inspect(response));

    let transcription = response.results.map(result => result.alternatives[0].transcript).join('\n');
    console.log(transcription);

    // The metadata value of the completed operation.
    let metadata = final_response[1];
    console.log(util.inspect(metadata));

    // The response of the api call returning the complete operation.
    let finalApiResponse = final_response[2];
    console.log(util.inspect(finalApiResponse));
    
    //https://cloud.google.com/nodejs/docs/reference/storage/2.0.x/File.html#save
    let file_name = path.basename(object.name);
    console.log('Write to: "'+file_name+'.txt"');
    let file = bucket.file('#txt/'+file_name+'.txt');
    
    await file.save(transcription, {
      contentType: 'text/plain',
      resumable: false
    });

  } catch (error) {
    console.error(util.inspect(error));
  }
});


exports.processAudioMetadata = functions.region('europe-west1').runWith({timeoutSeconds: 540})
.storage.object().onFinalize(async (object) => {
  try {
    if (!object.name.startsWith('metadata/')) return console.log('Invalid "folder"!');
    if (!object.contentType.startsWith('audio/')) return console.log('Invalid "contentType"!');
    
    let audio_buffer = await bucket.file(object.name).download();
    let audio_metadata = await mm.parseBuffer(audio_buffer[0], object.contentType);
    console.log(util.inspect(audio_metadata));

    let config = {
      encoding: formats[audio_metadata.format.dataformat], // OGG_OPUS  FLAC 
      sampleRateHertz: audio_metadata.format.sampleRate,
      languageCode: 'de-DE',
      enableAutomaticPunctuation: true,
      useEnhanced: true,
      model: 'default',
      metadata: {
        originalMediaType: 'AUDIO',
        interactionType: 'PHONE_CALL',
        recordingDeviceType: 'PHONE_LINE',
        microphoneDistance: 'NEARFIELD'
      }
    }
    if (audio_metadata.format.numberOfChannels > 1) {
      config.enableSeparateRecognitionPerChannel = true;
      config.audioChannelCount = audio_metadata.format.numberOfChannels;
    } else {
      config.enableSpeakerDiarization = true;
      config.diarizationSpeakerCount = 2;
    }

    let initial_response = await speech.longRunningRecognize({
      audio: {
        uri: `gs://${object.bucket}/${object.name}`,
      },
      config: config
    });
    let operation = initial_response[0];
    let initialApiResponse = initial_response[1];
    console.log(util.inspect(operation));
    console.log(util.inspect(initialApiResponse));

    // Get a Promise representation of the final result of the job
    let final_response = await operation.promise();

    // The final result of the operation.
    let response = final_response[0];
    console.log(util.inspect(response));

    let transcription = null;
    if (config.enableSeparateRecognitionPerChannel) {
      transcription = response.results.map(result => "Channel "+result.channelTag+": "+result.alternatives[0].transcript).join('\n');
    } else {
      transcription = response.results.map(result => result.alternatives[0].transcript).join('\n');
    }
    console.log(transcription);

    if (config.enableSpeakerDiarization) {
      /*
      https://cloud.google.com/speech-to-text/docs/multiple-voices
      Speaker Diarization
      Note: The transcript within each result is separate and sequential per result.
      However, the words list within an alternative includes all the words
      from all the results thus far. Thus, to get all the words with speaker
      tags, you only have to take the words list from the last result:
      */
      let last_result = response.results[response.results.length - 1];
      let words = last_result.alternatives[0].words;
      let words_info = words.map(word_info => `word: ${word_info.word}, speakerTag: ${word_info.speakerTag}`).join(' ; ');
      console.log(words_info);
      transcription = transcription+'\n\n\n'+words_info;
    }

    // The metadata value of the completed operation.
    let metadata = final_response[1];
    console.log(util.inspect(metadata));

    // The response of the api call returning the complete operation.
    let finalApiResponse = final_response[2];
    console.log(util.inspect(finalApiResponse));
    
    //https://cloud.google.com/nodejs/docs/reference/storage/2.0.x/File.html#save
    let file_name = path.basename(object.name);
    console.log('Write to: "'+file_name+'.txt"');
    let file = bucket.file('#txt/'+file_name+'.txt');
    
    await file.save(transcription, {
      contentType: 'text/plain',
      resumable: false
    });

  } catch (error) {
    console.error(util.inspect(error));
  }
});